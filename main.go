package main

import (
	"fmt"
        "os"
	"log"
	"net/http"
	"time"
        "github.com/fatih/color"
)

func handler(w http.ResponseWriter, r *http.Request) {
	// Arbitrary sleep so that we can demonstrate autoscaler
	time.Sleep(100 * time.Millisecond)

        name, err := os.Hostname()
        if err != nil {
            panic(err)
        }

        green := color.New(color.FgGreen).SprintFunc()
        yellow := color.New(color.FgYellow).SprintFunc()
        red := color.New(color.FgRed).SprintFunc()
        //blue := color.New(color.FgBlue).SprintFunc()

        fmt.Printf("This is a %s and this is %s.\n", yellow("warning"), red("error"))


	fmt.Fprintln(w, "Hi there, I'm running in Kubernetes Pod: ", green(name))
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
